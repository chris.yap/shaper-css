import pkg from './package'

// module.exports = {
export default {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
    '@/static/sass/shaper.sass'
    // '@/static/sass/shaper-fontpath.sass'
    // '@/static/sass/codepen-embed.css'
    // '@/static/sass/utilities/_all.sass',
    // '@/static/sass/nabtrade.sass'
  ],
  /*
  ** Nuxt.js modules
  */
  module: [
    // rules: [
    //   {
    //     test: /\.sass$/,
    //     use: ExtractTextPlugin.extract({
    //       fallback: 'style-loader',
    //       use: ['css-loader', 'sass-loader']
    //     })
    //   }
    // ]
    ['nuxt-sass-resources-loader', '@/static/sass/shaper.sass']
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    // new ExtractTextPlugin('css/styles.css')
  ],
  /*
  ** Build configuration
  */
  build: {
    // analyze: true,
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      //   // Run ESLint on save
      //   if (ctx.isDev && ctx.isClient) {
      //     config.module.rules.push({
      //       enforce: 'pre',
      //       test: /\.(js|vue)$/,
      //       loader: 'eslint-loader',
      //       exclude: /(node_modules)/
      //     })
      //   }
    },
    postcss: {
      preset: {
        autoprefixer: {
          grid: true
        }
      }
    },
    extractCSS: true,
    filenames: {
      css: 'css/[name].css'
    }
  }
}
